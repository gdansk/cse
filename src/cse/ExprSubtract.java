package cse;

public class ExprSubtract extends ExprBinaryOperator {
	public ExprSubtract(Expr left, Expr right){
		super(left, right, "-", 10);
	}

	@Override
	public Expr evaluate(SpreadsheetModel env) {
		return new ExprReal(getLeft().evaluate(env).toReal() - getRight().evaluate(env).toReal());
	}

	@Override
	public Expr translate(int coffset, int roffset) {
		return new ExprSubtract(getLeft().translate(coffset, roffset), getRight().translate(coffset, roffset));
	}
}
