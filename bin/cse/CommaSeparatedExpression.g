grammar CommaSeparatedExpression;

line 
  : expr # CellOutput
  ;
  
expr
  : L_PAREN expr R_PAREN # Grouped
  | UMINUS expr # UnaryNegate
  | expr POWER expr # Power
  | expr MULTI_OPS expr # Multiply
  | expr ADD_OPS expr # Add
  | COLLECT_OPS L_PAREN IDENTIFIER ':' IDENTIFIER R_PAREN # Collection
  | DIGITS # Literal
  | IDENTIFIER # Address
  ;
  
L_PAREN: '(';
R_PAREN: ')';
POWER: '^';
MULTI_OPS: [*/%];
ADD_OPS: '- ' | '+';
COLLECT_OPS: 'mean' | 'sum' | 'max' | 'min';
UMINUS: '-';
IDENTIFIER: '$'? [A-Z]+ '$'? [0-9]+;
DIGITS: '-'? [0-9]* '.'? [0-9]+;

WHITESPACE: [ \n\r\t]+ -> skip;