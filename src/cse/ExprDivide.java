package cse;

public class ExprDivide extends ExprBinaryOperator {
	public ExprDivide(Expr left, Expr right){
		super(left, right, "/", 20);
	}

	@Override
	public Expr evaluate(SpreadsheetModel env) {
		return new ExprReal(getLeft().evaluate(env).toReal() / getRight().evaluate(env).toReal());
	}

	@Override
	public Expr translate(int coffset, int roffset) {
		return new ExprDivide(getLeft().translate(coffset, roffset), getRight().translate(coffset, roffset));
	}
}
