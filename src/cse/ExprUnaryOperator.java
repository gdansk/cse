package cse;

public abstract class ExprUnaryOperator extends Expr {
	private Expr subexpr;
	protected Expr getSubexpr() {
		return subexpr;
	}

	private String symbol;
	public ExprUnaryOperator(Expr subexpr, String symbol, int precedence){
		super(precedence);
		this.subexpr = subexpr;
		this.symbol = symbol;
	}
	
	@Override
	public String toSource(){
		boolean lower = subexpr.getPrecedence() < getPrecedence();
		return symbol + (lower ? "(" : "") + subexpr.toSource() + (lower ? ")" : "");
	}
}
