package cse;

public class ExprSum extends ExprFunctionCall {
	public ExprSum(Address from, Address to){
		super(from, to, "sum");
	}

	@Override
	public Expr evaluate(SpreadsheetModel env) {
		int columnInc = 0;
		int rowInc = 0;
		if(getTo().getColumn() > getFrom().getColumn()){
			columnInc = 1;
		}
		if(getTo().getRow() > getFrom().getRow()){
			rowInc = 1;
		}
		int column = getFrom().getColumn();
		int row = getFrom().getRow();
		double sum = 0;
		while(row <= getTo().getRow() && column <= getTo().getColumn()){
			sum += env.evaluate(column, row).toReal();
			column += columnInc;
			row += rowInc;
		}
		return new ExprReal(sum);
	}

	@Override
	public Expr translate(int coffset, int roffset) {
		return new ExprSum(getFrom().translate(coffset, roffset), getTo().translate(coffset, roffset));
	}
}
