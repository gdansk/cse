package cse;

public class ExprAddress extends ExprPrimitive {
	private Address address;
	public ExprAddress(Address add){
		super();
		address = add;
	}
	
	@Override
	public Expr evaluate(SpreadsheetModel env) {
		return env.evaluate(address.getColumn(), address.getRow());
	}
	
	@Override
	public Expr translate(int coffset, int roffset) {
		return new ExprAddress(address.translate(coffset, roffset));
	}
	
	@Override
	public String toSource() {
		return address.toString();
	}
	
	@Override
	public String toString() {
		return address.toString();
	}
}
