package cse;

public class ExprInteger extends ExprPrimitive {
	private int value;
	public ExprInteger(int val){
		super();
		this.value = val;
	}
	
	@Override
	public int toInteger(){ 
		return value;
	}
	
	@Override	
	public double toReal(){
		return (double)value;
	}
	
	@Override
	public String toString(){
		return Integer.toString(value);
	}

	@Override
	public Expr evaluate(SpreadsheetModel env) {
		return this;
	}

	@Override
	public Expr translate(int coffset, int roffset) {
		return this;
	}
}
