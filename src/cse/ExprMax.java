package cse;

public class ExprMax extends ExprFunctionCall {
	public ExprMax(Address from, Address to){
		super(from, to, "max");
	}

	@Override
	public Expr evaluate(SpreadsheetModel env) {
		int columnInc = 0;
		int rowInc = 0;
		if(getTo().getColumn() > getFrom().getColumn()){
			columnInc = 1;
		}
		if(getTo().getRow() > getFrom().getRow()){
			rowInc = 1;
		}
		int column = getFrom().getColumn();
		int row = getFrom().getRow();
		double max = Double.MIN_VALUE;
		double cur = 0;
		while(row <= getTo().getRow() && column <= getTo().getColumn()){
			cur = env.evaluate(column, row).toReal();
			if(cur > max){
				max = cur;
			}
			column += columnInc;
			row += rowInc;
		}
		return new ExprReal(max);
	}

	@Override
	public Expr translate(int coffset, int roffset) {
		return new ExprMax(getFrom().translate(coffset, roffset), getTo().translate(coffset, roffset));
	}
}
