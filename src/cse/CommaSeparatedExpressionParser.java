// Generated from cse/CommaSeparatedExpression.g by ANTLR 4.5
package cse;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CommaSeparatedExpressionParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, L_PAREN=2, R_PAREN=3, POWER=4, MULTI_OPS=5, ADD_OPS=6, COLLECT_OPS=7, 
		UMINUS=8, IDENTIFIER=9, DIGITS=10, WHITESPACE=11;
	public static final int
		RULE_line = 0, RULE_expr = 1;
	public static final String[] ruleNames = {
		"line", "expr"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "':'", "'('", "')'", "'^'", null, null, null, "'-'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, "L_PAREN", "R_PAREN", "POWER", "MULTI_OPS", "ADD_OPS", "COLLECT_OPS", 
		"UMINUS", "IDENTIFIER", "DIGITS", "WHITESPACE"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "CommaSeparatedExpression.g"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public CommaSeparatedExpressionParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class LineContext extends ParserRuleContext {
		public LineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_line; }
	 
		public LineContext() { }
		public void copyFrom(LineContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class CellOutputContext extends LineContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public CellOutputContext(LineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).enterCellOutput(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).exitCellOutput(this);
		}
	}

	public final LineContext line() throws RecognitionException {
		LineContext _localctx = new LineContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_line);
		try {
			_localctx = new CellOutputContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(4);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AddContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ADD_OPS() { return getToken(CommaSeparatedExpressionParser.ADD_OPS, 0); }
		public AddContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).enterAdd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).exitAdd(this);
		}
	}
	public static class AddressContext extends ExprContext {
		public TerminalNode IDENTIFIER() { return getToken(CommaSeparatedExpressionParser.IDENTIFIER, 0); }
		public AddressContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).enterAddress(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).exitAddress(this);
		}
	}
	public static class LiteralContext extends ExprContext {
		public TerminalNode DIGITS() { return getToken(CommaSeparatedExpressionParser.DIGITS, 0); }
		public LiteralContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).exitLiteral(this);
		}
	}
	public static class MultiplyContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode MULTI_OPS() { return getToken(CommaSeparatedExpressionParser.MULTI_OPS, 0); }
		public MultiplyContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).enterMultiply(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).exitMultiply(this);
		}
	}
	public static class CollectionContext extends ExprContext {
		public TerminalNode COLLECT_OPS() { return getToken(CommaSeparatedExpressionParser.COLLECT_OPS, 0); }
		public TerminalNode L_PAREN() { return getToken(CommaSeparatedExpressionParser.L_PAREN, 0); }
		public List<TerminalNode> IDENTIFIER() { return getTokens(CommaSeparatedExpressionParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(CommaSeparatedExpressionParser.IDENTIFIER, i);
		}
		public TerminalNode R_PAREN() { return getToken(CommaSeparatedExpressionParser.R_PAREN, 0); }
		public CollectionContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).enterCollection(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).exitCollection(this);
		}
	}
	public static class GroupedContext extends ExprContext {
		public TerminalNode L_PAREN() { return getToken(CommaSeparatedExpressionParser.L_PAREN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode R_PAREN() { return getToken(CommaSeparatedExpressionParser.R_PAREN, 0); }
		public GroupedContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).enterGrouped(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).exitGrouped(this);
		}
	}
	public static class UnaryNegateContext extends ExprContext {
		public TerminalNode UMINUS() { return getToken(CommaSeparatedExpressionParser.UMINUS, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public UnaryNegateContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).enterUnaryNegate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).exitUnaryNegate(this);
		}
	}
	public static class PowerContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode POWER() { return getToken(CommaSeparatedExpressionParser.POWER, 0); }
		public PowerContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).enterPower(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CommaSeparatedExpressionListener ) ((CommaSeparatedExpressionListener)listener).exitPower(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 2;
		enterRecursionRule(_localctx, 2, RULE_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(21);
			switch (_input.LA(1)) {
			case UMINUS:
				{
				_localctx = new UnaryNegateContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(7);
				match(UMINUS);
				setState(8);
				expr(7);
				}
				break;
			case L_PAREN:
				{
				_localctx = new GroupedContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(9);
				match(L_PAREN);
				setState(10);
				expr(0);
				setState(11);
				match(R_PAREN);
				}
				break;
			case COLLECT_OPS:
				{
				_localctx = new CollectionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(13);
				match(COLLECT_OPS);
				setState(14);
				match(L_PAREN);
				setState(15);
				match(IDENTIFIER);
				setState(16);
				match(T__0);
				setState(17);
				match(IDENTIFIER);
				setState(18);
				match(R_PAREN);
				}
				break;
			case DIGITS:
				{
				_localctx = new LiteralContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(19);
				match(DIGITS);
				}
				break;
			case IDENTIFIER:
				{
				_localctx = new AddressContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(20);
				match(IDENTIFIER);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(34);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(32);
					switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
					case 1:
						{
						_localctx = new PowerContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(23);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(24);
						match(POWER);
						setState(25);
						expr(7);
						}
						break;
					case 2:
						{
						_localctx = new MultiplyContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(26);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(27);
						match(MULTI_OPS);
						setState(28);
						expr(6);
						}
						break;
					case 3:
						{
						_localctx = new AddContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(29);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(30);
						match(ADD_OPS);
						setState(31);
						expr(5);
						}
						break;
					}
					} 
				}
				setState(36);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 6);
		case 1:
			return precpred(_ctx, 5);
		case 2:
			return precpred(_ctx, 4);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\r(\4\2\t\2\4\3\t"+
		"\3\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\5\3\30\n\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3#\n\3\f\3\16\3&\13"+
		"\3\3\3\2\3\4\4\2\4\2\2,\2\6\3\2\2\2\4\27\3\2\2\2\6\7\5\4\3\2\7\3\3\2\2"+
		"\2\b\t\b\3\1\2\t\n\7\n\2\2\n\30\5\4\3\t\13\f\7\4\2\2\f\r\5\4\3\2\r\16"+
		"\7\5\2\2\16\30\3\2\2\2\17\20\7\t\2\2\20\21\7\4\2\2\21\22\7\13\2\2\22\23"+
		"\7\3\2\2\23\24\7\13\2\2\24\30\7\5\2\2\25\30\7\f\2\2\26\30\7\13\2\2\27"+
		"\b\3\2\2\2\27\13\3\2\2\2\27\17\3\2\2\2\27\25\3\2\2\2\27\26\3\2\2\2\30"+
		"$\3\2\2\2\31\32\f\b\2\2\32\33\7\6\2\2\33#\5\4\3\t\34\35\f\7\2\2\35\36"+
		"\7\7\2\2\36#\5\4\3\b\37 \f\6\2\2 !\7\b\2\2!#\5\4\3\7\"\31\3\2\2\2\"\34"+
		"\3\2\2\2\"\37\3\2\2\2#&\3\2\2\2$\"\3\2\2\2$%\3\2\2\2%\5\3\2\2\2&$\3\2"+
		"\2\2\5\27\"$";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}