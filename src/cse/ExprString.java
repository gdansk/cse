package cse;

public class ExprString extends ExprPrimitive {
	private String value;
	public ExprString(String val){
		super();
		this.value = val;
	}
	
	@Override
	public int toInteger(){ 
		return (int)toReal();
	}
	
	@Override	
	public double toReal(){
		return Double.parseDouble(value);
	}
	
	@Override
	public String toString(){
		return value;
	}

	@Override
	public Expr evaluate(SpreadsheetModel env) {
		return this;
	}

	@Override
	public Expr translate(int coffset, int roffset) {
		return this;
	}
}
