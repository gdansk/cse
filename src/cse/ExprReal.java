package cse;

public class ExprReal extends ExprPrimitive {
	private double value;
	public ExprReal(double val){
		super();
		this.value = val;
	}
	
	@Override
	public int toInteger(){ 
		return (int)value;
	}
	
	@Override	
	public double toReal(){
		return value;
	}
	
	@Override
	public String toString(){
		return Double.toString(value);
	}

	@Override
	public Expr evaluate(SpreadsheetModel env) {
		return this;
	}

	@Override
	public Expr translate(int coffset, int roffset) {
		return this;
	}
}
