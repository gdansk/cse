package cse;

public abstract class ExprBinaryOperator extends Expr{
	private Expr left;
	private Expr right;
	private String symbol;
	protected Expr getLeft() {
		return left;
	}

	protected Expr getRight() {
		return right;
	}

	public ExprBinaryOperator(Expr left, Expr right, String symbol, int precedence){
		super(precedence);
		this.left = left;
		this.right = right;
		this.symbol = symbol;
	}
	
	@Override
	public String toSource(){
		boolean leftLower  = left.getPrecedence() < getPrecedence();
		boolean rightLower = right.getPrecedence() < getPrecedence();
		return (leftLower ? "(" : "") + left.toSource() + (leftLower ? ")" : "") 
				+ ' ' + symbol + ' ' 
				+ (rightLower ? "(" : "") + right.toSource() + (rightLower ? ")" : "");
	}
}
