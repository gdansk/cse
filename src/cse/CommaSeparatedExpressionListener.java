// Generated from cse/CommaSeparatedExpression.g by ANTLR 4.5
package cse;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link CommaSeparatedExpressionParser}.
 */
public interface CommaSeparatedExpressionListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code CellOutput}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#line}.
	 * @param ctx the parse tree
	 */
	void enterCellOutput(CommaSeparatedExpressionParser.CellOutputContext ctx);
	/**
	 * Exit a parse tree produced by the {@code CellOutput}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#line}.
	 * @param ctx the parse tree
	 */
	void exitCellOutput(CommaSeparatedExpressionParser.CellOutputContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Add}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterAdd(CommaSeparatedExpressionParser.AddContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Add}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitAdd(CommaSeparatedExpressionParser.AddContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Address}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterAddress(CommaSeparatedExpressionParser.AddressContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Address}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitAddress(CommaSeparatedExpressionParser.AddressContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Literal}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(CommaSeparatedExpressionParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Literal}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(CommaSeparatedExpressionParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Multiply}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterMultiply(CommaSeparatedExpressionParser.MultiplyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Multiply}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitMultiply(CommaSeparatedExpressionParser.MultiplyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Collection}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterCollection(CommaSeparatedExpressionParser.CollectionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Collection}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitCollection(CommaSeparatedExpressionParser.CollectionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Grouped}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterGrouped(CommaSeparatedExpressionParser.GroupedContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Grouped}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitGrouped(CommaSeparatedExpressionParser.GroupedContext ctx);
	/**
	 * Enter a parse tree produced by the {@code UnaryNegate}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterUnaryNegate(CommaSeparatedExpressionParser.UnaryNegateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code UnaryNegate}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitUnaryNegate(CommaSeparatedExpressionParser.UnaryNegateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Power}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterPower(CommaSeparatedExpressionParser.PowerContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Power}
	 * labeled alternative in {@link CommaSeparatedExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitPower(CommaSeparatedExpressionParser.PowerContext ctx);
}