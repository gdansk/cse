package cse;
//import java.util.HashMap;
import java.util.Stack;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import cse.CommaSeparatedExpressionParser.AddContext;
import cse.CommaSeparatedExpressionParser.AddressContext;
import cse.CommaSeparatedExpressionParser.CellOutputContext;
import cse.CommaSeparatedExpressionParser.CollectionContext;
import cse.CommaSeparatedExpressionParser.GroupedContext;
import cse.CommaSeparatedExpressionParser.LiteralContext;
import cse.CommaSeparatedExpressionParser.MultiplyContext;
import cse.CommaSeparatedExpressionParser.PowerContext;
import cse.CommaSeparatedExpressionParser.UnaryNegateContext;

public class Interpreter implements CommaSeparatedExpressionListener {
	private Stack<Expr> operands;

	public Interpreter(){
		operands = new Stack<>();
	}
	
	public static Expr parseToAST(String source){
		System.out.println("Parsing: \n" + source);
		ANTLRInputStream ais = new ANTLRInputStream(source);
		CommaSeparatedExpressionLexer lexer = new CommaSeparatedExpressionLexer(ais);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		CommaSeparatedExpressionParser parser = new CommaSeparatedExpressionParser(tokens);
		ParseTree tree = parser.line();
		
		//errors
	    System.out.println("Errors: " + parser.getNumberOfSyntaxErrors());
		
		ParseTreeWalker walkerTexasRanger = new ParseTreeWalker();
		Interpreter interpreter = new Interpreter();
		walkerTexasRanger.walk(interpreter, tree);
		return interpreter.operands.peek();
	}

	@Override
	public void enterEveryRule(ParserRuleContext ctx) {
		// Nothing?
	}

	@Override
	public void exitEveryRule(ParserRuleContext ctx) {
		// Nothing?
		
	}

	@Override
	public void visitErrorNode(ErrorNode ctx) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visitTerminal(TerminalNode ctx) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enterCellOutput(CellOutputContext ctx) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void exitCellOutput(CellOutputContext ctx){
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enterAdd(AddContext ctx) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exitAdd(AddContext ctx) {
		Expr b = operands.pop();
		Expr a = operands.pop();
		if(ctx.ADD_OPS().getText().equals("+")){
			operands.push(new ExprAdd(a,b));
		} else {
			operands.push(new ExprSubtract(a,b));
		}
	}

	@Override
	public void enterAddress(AddressContext ctx) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exitAddress(AddressContext ctx) {
		operands.push(new ExprAddress(new Address(ctx.IDENTIFIER().getText())));
	}

	@Override
	public void enterLiteral(LiteralContext ctx) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exitLiteral(LiteralContext ctx) {
		if(ctx.DIGITS().getText().contains(".")){
			operands.push(new ExprReal(Double.parseDouble(ctx.DIGITS().getText())));
		} else {
			operands.push(new ExprInteger(Integer.parseInt(ctx.DIGITS().getText())));
		}
	}

	@Override
	public void enterMultiply(MultiplyContext ctx) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exitMultiply(MultiplyContext ctx) {
		Expr b = operands.pop();
		Expr a = operands.pop();
		
		if(ctx.MULTI_OPS().getText().equals("*")){
			operands.push(new ExprMultiply(a, b));
		} else if (ctx.MULTI_OPS().getText().equals("/")) {
			operands.push(new ExprDivide(a, b));
		} else {
			operands.push(new ExprRemainder(a, b));
		}
	}

	@Override
	public void enterCollection(CollectionContext ctx) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exitCollection(CollectionContext ctx) {
		String func = ctx.COLLECT_OPS().getText();
		Address a = new Address(ctx.IDENTIFIER().get(0).getText());
		Address b = new Address(ctx.IDENTIFIER().get(1).getText());
		Expr collection = null;
		switch(func){
			case "mean":
				collection = new ExprMean(a,b);
				break;
			case "min":
				collection = new ExprMin(a,b);
				break;
			case "max":
				collection = new ExprMax(a,b);
				break;
			case "sum":
				collection = new ExprSum(a,b);
				break;
		}
		operands.push(collection);
	}

	@Override
	public void enterGrouped(GroupedContext ctx) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exitGrouped(GroupedContext ctx) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void enterUnaryNegate(UnaryNegateContext ctx) {
		// TODO Auto-generated method stub
	}

	@Override
	public void exitUnaryNegate(UnaryNegateContext ctx) {
		Expr a = operands.pop();
		operands.push(new ExprNegate(a));
	}

	@Override
	public void enterPower(PowerContext ctx) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void exitPower(PowerContext ctx) {
		Expr b = operands.pop();
		Expr a = operands.pop();
		operands.push(new ExprPower(a,b));
	}
}
