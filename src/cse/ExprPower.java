package cse;

public class ExprPower extends ExprBinaryOperator {
	public ExprPower(Expr left, Expr right){
		super(left, right, "^", 30);
	}

	@Override
	public Expr evaluate(SpreadsheetModel env) {
		return new ExprReal(Math.pow(getLeft().evaluate(env).toReal(), getRight().evaluate(env).toReal()));
	}

	@Override
	public Expr translate(int coffset, int roffset) {
		return new ExprPower(getLeft().translate(coffset, roffset), getRight().translate(coffset, roffset));
	}
}
