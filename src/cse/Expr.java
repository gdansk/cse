package cse;

public abstract class Expr {
	private int precedence;
	public Expr(int precedence){
		this.precedence = precedence;
	}
	public abstract Expr evaluate(SpreadsheetModel env);
	public abstract Expr translate(int coffset, int roffset);
	public abstract String toSource();
	public int toInteger(){
		throw new UnsupportedOperationException();
	}
	public double toReal(){
		throw new UnsupportedOperationException();
	}
	public String toString(){
		throw new UnsupportedOperationException();
	}
	public int getPrecedence(){
		return precedence;
	}
}
