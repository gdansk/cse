package cse;

public abstract class ExprFunctionCall extends Expr {
	private Address from;
	private Address to;
	private String name;
	protected Address getFrom() {
		return from;
	}

	protected Address getTo() {
		return to;
	}

	public ExprFunctionCall(Address from, Address to, String name){
		super(50);
		this.from = from;
		this.to = to;
		this.name = name;
	}
	
	@Override
	public String toSource(){
		return name + '(' + from.toString() + ':' + to.toString() + ')';
	}
}
