package cse;

public class ExprNegate extends ExprUnaryOperator {

	public ExprNegate(Expr subexpr) {
		super(subexpr, "-", 40);
	}

	@Override
	public Expr evaluate(SpreadsheetModel env) {
		return new ExprReal(-getSubexpr().evaluate(env).toReal());
	}

	@Override
	public Expr translate(int coffset, int roffset) {
		return new ExprNegate(getSubexpr().translate(coffset, roffset));
	}

}
