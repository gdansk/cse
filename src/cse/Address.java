package cse;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Address {
	private int mRow;
	private int mColumn;
	private boolean lockedColumn;
	private boolean lockedRow;
	//int value;
	public Address(int column, boolean columnLock, int row, boolean rowLock){
		mRow = row;
		mColumn = column;
		lockedColumn = columnLock;
		lockedRow = rowLock;
	}
	public Address(String descriptor){
		Pattern p = Pattern.compile("(\\$?)([A-Z])(\\$?)(\\d{1,3})");
		Matcher m = p.matcher(descriptor);
		m.find();	
		lockedColumn = m.group(1).length() > 0;
		lockedRow = m.group(3).length() > 0;

		mColumn = ((int)m.group(2).charAt(0)) - 65;// - Character.getNumericValue('A');
		mRow = Integer.parseInt(m.group(4));
	}
	public int getRow(){
		return mRow;
	}
	public int getColumn(){
		return mColumn;
	}
	public Address translate(int coffset, int roffset){
		int newColumn = lockedColumn ? 0 : coffset;
		int newRow = lockedRow ? 0 : roffset;
		return new Address(mColumn + newColumn, lockedColumn, mRow + newRow, lockedRow);
	}
	@Override
	public String toString(){
		String out = "";
		if(lockedColumn){
			out += "$";
		}
		out += (char)(mColumn + 65);
		if (lockedRow){
			out += "$";
		}
		out += mRow;
		return out;
	}
}
