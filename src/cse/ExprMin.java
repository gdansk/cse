package cse;

public class ExprMin extends ExprFunctionCall {
	public ExprMin(Address from, Address to){
		super(from, to, "min");
	}

	@Override
	public Expr evaluate(SpreadsheetModel env) {
		int columnInc = 0;
		int rowInc = 0;
		if(getTo().getColumn() > getFrom().getColumn()){
			columnInc = 1;
		}
		if(getTo().getRow() > getFrom().getRow()){
			rowInc = 1;
		}
		int column = getFrom().getColumn();
		int row = getFrom().getRow();
		double min = Double.MAX_VALUE;
		double cur = 0;
		while(row <= getTo().getRow() && column <= getTo().getColumn()){
			cur = env.evaluate(column, row).toReal();
			if(cur < min){
				min = cur;
			}
			column += columnInc;
			row += rowInc;
		}
		return new ExprReal(min);
	}

	@Override
	public Expr translate(int coffset, int roffset) {
		return new ExprMin(getFrom().translate(coffset, roffset), getTo().translate(coffset, roffset));
	}
}
